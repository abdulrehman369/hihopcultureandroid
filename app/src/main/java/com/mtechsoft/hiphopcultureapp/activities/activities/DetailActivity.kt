package com.mtechsoft.hiphopcultureapp.activities.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mtechsoft.hiphopcultureapp.R
import com.mtechsoft.hiphopcultureapp.activities.utils.Utilities
import com.mtechsoft.hiphopcultureapp.databinding.ActivityDetailBinding
import com.squareup.picasso.Picasso


class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    lateinit var title: String
    lateinit var url: String
    lateinit var url1: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        title = Utilities.getString(this, "title").toString()
        url = Utilities.getString(this, "url").toString()
        url1 = Utilities.getString(this, "url1").toString()
        var url2: String = Utilities.getString(this, "url2").toString()
        var url3: String = Utilities.getString(this, "url3").toString()
        if (title.equals("Editorial")) {
            binding.tvUrl1.text = url1
            Picasso.get().load(R.drawable.editorial_app_icon).into(binding.ivimage)

        }
        if (title.equals("SnekrVursal")) {
            binding.tvUrl1.text = url1
            binding.tvUrl2.text = url2
            binding.tvUrl3.text = url3
            Picasso.get().load(R.drawable.snekrsvursal_app_icon).into(binding.ivimage)

        }
        if (title.equals("Photography")){
            Picasso.get().load(R.drawable.photography_detail_ic).into(binding.ivimage)

        }

        if (title.equals("Mics x Cameras")){
            Picasso.get().load(R.drawable.micsxcameras_app_icon).into(binding.ivimage)

        }
        
        binding.tvUrl.text = url
        binding.tvTitle.text = title
        binding.ivBack.setOnClickListener {
            finish()
        }
        binding.tvUrl.setOnClickListener {
            GoToURL(url)
        }
        binding . tvUrl1 . setOnClickListener {
            GoToURL(url1)
        }
        binding . tvUrl2 . setOnClickListener {
            GoToURL(url2)
        }
        binding . tvUrl3 . setOnClickListener {
            GoToURL(url3)
        }
    }

    fun GoToURL(url: String?) {
        val uri: Uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }
}