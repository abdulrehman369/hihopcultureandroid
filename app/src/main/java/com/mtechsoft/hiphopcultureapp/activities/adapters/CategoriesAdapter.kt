package com.mtech.travces.view.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mtechsoft.hiphopcultureapp.R
import com.mtechsoft.hiphopcultureapp.activities.activities.DetailActivity
import com.mtechsoft.hiphopcultureapp.activities.models.CategoriesModel
import com.mtechsoft.hiphopcultureapp.activities.utils.Utilities
import com.squareup.picasso.Picasso


class CategoriesAdapter(
    var context: Context,
    var list: List<CategoriesModel>
) :
    RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(container: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_categoreis,
                container,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) = holder.bind(i)

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvTitle = itemView.findViewById(R.id.tvTitle) as TextView
        var ivimage = itemView.findViewById(R.id.ivimage) as ImageView
        var layout_item = itemView.findViewById(R.id.layout_item) as LinearLayout


        fun bind(pos: Int) {
            Picasso.get().load(list[pos].image).into(ivimage)
            tvTitle.text = list[pos].title
            layout_item.setOnClickListener {
                var getTitle: String = tvTitle.text.toString()
                if (getTitle.equals("Hip Hop Culture")) {
                    Utilities.saveString(context, "url", "https://uhhm.org/")
                } else if (getTitle.equals("Photography")) {
                    Utilities.saveString(context, "url", "http://stellamagloire.com/")
                } else if (getTitle.equals("Editorial")) {
                    Utilities.saveString(context, "url", "https://www.xxlmag.com/")
                    Utilities.saveString(context, "url1", "https://allhiphop.com/")
                } else if (getTitle.equals("Mics x Cameras")) {
                    Utilities.saveString(context, "url", "https://uhhm.org/")
                } else if (getTitle.equals("SnekrVursal")) {
                    Utilities.saveString(context, "url", "https://thentwrk.com/")
                    Utilities.saveString(context, "url1", "https://www.kyx.world/?gclid=Cj0KCQjw6NmHBhD2ARIsAI3hrM2c_xDuBqfkB_PVL2EO4JEf8-5wM3tbd7qnXMhpp0qWUh3HQlxvwVcaAqEBEALw_wcB")
                    Utilities.saveString(context, "url2", "https://www.dropp.tv/")
                    Utilities.saveString(context, "url3", "https://extrabutterny.com/")
                }
                Utilities.saveString(context, "title", tvTitle.text.toString())
                context.startActivity(Intent(context, DetailActivity::class.java))
            }
//            initClickListeners()
        }

//        private fun initClickListeners() {
//            layout_item.setOnClickListener {
//                callback.onItemClicked(adapterPosition)
//
//            }
//        }
    }

    interface Callback {
        fun onItemClicked(pos: Int)
//        fun onDeleteClicked(pos: Int)
//        fun oncvItemClicked(pos: Int)
    }
}