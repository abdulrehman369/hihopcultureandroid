package com.mtechsoft.hiphopcultureapp.activities.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.mtechsoft.hiphopcultureapp.R


class SplashActivity : AppCompatActivity() {
    private val progressBar: ProgressBar? = null
    private val pStatus = 0
    private val handler = Handler()
    private val SPLASH_TIME_OUT = 2000L
    lateinit var login_status: String
    var isLogin:String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed(
            {
                val i = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(i)
                finish()
            }, SPLASH_TIME_OUT
        )

    }
}
