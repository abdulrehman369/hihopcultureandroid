package com.mtechsoft.hiphopcultureapp.activities.activities

import android.R.attr.spacing
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.mtech.travces.view.adapters.CategoriesAdapter
import com.mtechsoft.hiphopcultureapp.R
import com.mtechsoft.hiphopcultureapp.activities.models.CategoriesModel
import com.mtechsoft.hiphopcultureapp.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        addData()
    }

    private fun addData() {

        binding.rvYouWillNeed.layoutManager =
            GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        binding.rvYouWillNeed.setHasFixedSize(true)

        val CategoriesModel = ArrayList<CategoriesModel>()



        CategoriesModel.add(
            CategoriesModel(
                "Hip Hop Culture", R.drawable.hiphopculture_ic
            )
        )

        CategoriesModel.add(
            CategoriesModel(
                "Photography", R.drawable.photography_ic
            )
        )

        CategoriesModel.add(
            CategoriesModel(
                "Editorial", R.drawable.editorial_app_icon
            )
        )

        CategoriesModel.add(
            CategoriesModel(
                "Mics x Cameras", R.drawable.micsxcameras_app_icon
            )
        )

        CategoriesModel.add(
            CategoriesModel(
                "SnekrVursal", R.drawable.snekrsvursal_app_icon
            )
        )
        val youWillNeeddAdapter = CategoriesAdapter(this, CategoriesModel)
        binding.rvYouWillNeed.adapter = youWillNeeddAdapter
    }
}