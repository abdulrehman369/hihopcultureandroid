package com.mtechsoft.hiphopcultureapp.activities.utils

import android.content.Context

class Utilities {
    companion object {
        fun saveString(context: Context, key: String?, value: String?) {
            val sharedPref =
                    context.getSharedPreferences("ReadingSharedStorage", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putString(key, value)
            editor.apply()
        }

        fun saveBoolean(context: Context, key: String?, value: Boolean) {
            val sharedPref =
                    context.getSharedPreferences("ReadingSharedStorage", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }

        fun getString(context: Context, key: String?): String? {
            val sharedPref =
                    context.getSharedPreferences("ReadingSharedStorage", Context.MODE_PRIVATE)
            return sharedPref.getString(key, "")
        }

        fun getBoolean(context: Context, key: String?): Boolean {
            val sharedPref =
                    context.getSharedPreferences("ReadingSharedStorage", Context.MODE_PRIVATE)
            return sharedPref.getBoolean(key, false)
        }

        fun clearSharedPref(context: Context) {
            val sharedPref =
                    context.getSharedPreferences("ReadingSharedStorage", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.clear()
            editor.apply()
        }

    }


}